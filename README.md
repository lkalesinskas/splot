# README #

S-plot2 creates an interactive, two-dimensional heatmap capturing the similarities and dissimilarities in nucleotide usage between genomic sequences (partial or complete). In S-plot2, whole eukaryotic chromosomes and smaller prokaryotic genomes can be efficiently compared. The tool includes functionality to extract, analyze, and automate BLAST queries of regions of interest within the heatmap. This facilitates the investigation of quickly evolving coding regions, novel coding regions, and laterally transferred elements.

S-plot2 is fully functional in the Windows and UNIX operating system. Due to the lack of support for Compatibility profiles on MacOS, rendering and maneuvering within the S-plot2 heatmap is suboptimal on MacOS.

Please view our Tutorial PDF, included in the download, for detailed instructions on how to run and use SPlot.

S-plot2 is licensed under a GPLv3 License.